# Contributing

Hi! Thanks for being willing to contribute.

Add some issues, suggest fixes for issues, etc. 

Remember it's GPLv3 so anybody who shares any part of the project has to share
the full source as well.
