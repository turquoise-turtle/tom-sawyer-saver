# TomSawyerSaver

A chrome extension that lets you play games from Coolmath Games in fullscreen

## Install:

1. Download the [latest zip](https://gitlab.com/turquoise-turtle/tom-sawyer-saver/-/jobs/artifacts/master/download?job=job1)
2. Unzip the folder
3. Go to chrome://extensions
4. Check Developer Mode
5. Click Load Unpacked Extension
6. Choose the root folder of the unzipped extension

## Use it:

1. Click the top hat icon on Coolmath Games or when you're already on a fullscreen flash game

## [Contributing](CONTRIBUTING.md)